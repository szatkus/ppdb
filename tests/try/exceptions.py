class MyException(Exception):

  a = 'my'

  def __init__(self):
    super().__init__()


try:
  print('here')
  raise MyException()
  print('not here')
except MyException as e:
  print(e.a)

print('and here')
