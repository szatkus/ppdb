def gen(y):
  x = y + 2
  print('before yield')
  yield x
  print('after yield')
  while x < 20:
    x += 1
    yield x

a = gen(13)
print('here')
for g in a:
  print(g)
