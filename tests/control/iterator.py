class I:
  x = 0
  def __iter__(self):
    return self
  def __next__(self):
    if self.x > 10:
      raise StopIteration
    self.x += 1
    return self.x

for i in I():
  print(i)
