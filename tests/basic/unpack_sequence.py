def f():
  return 1, 2, 'trzy'

a, _, b = f()
print(a)
print(b)

try:
  c, d = f()
except ValueError:
  print('ok')
