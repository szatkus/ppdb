a = 1
b = 2

def f():
  c = 3
  a = 5
  print(locals()['a'])
  print(locals()['c'])
  print(globals()['a'])
  print('c' not in globals())

f()
print(id(locals()) == id(globals()))
