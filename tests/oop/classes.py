class A:
  a = 7
  b = []
  def c(self):
    return self.a + 7

print(A.a)
print(A.b)
a = A()
print(a.a)
a.a = 9
print(a.a)
print(A.a)
a.b.append(1)
print(a.b)
print(A.b)
print(a.c())
f = a.c
print(f())
