class A:
  x = 1
  def f(self):
    return 77

class C:
  x = 2

class B(A, C):
  def g(self):
    return 23

A.x = 5
B.x = 6
a = B()
B.x = 7
print(a.x)
a.x= 99
print(a.x)
print(B.x)
print(a.f() + a.g())
