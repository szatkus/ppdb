class A:
  a = 5
  @classmethod
  def f(cls):
    return cls.a

print(A.f())