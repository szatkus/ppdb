class A:
  def f(self):
    print(7)
    print(self.x)

class B(A):
  def get_super(self):
    return super()

b = B()
b.x = 99
b.get_super().f()
