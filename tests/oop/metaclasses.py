class M(type):
  x = 7
  def __new__(cls, a, b, c):
    print('new')
    return super().__new__(cls, a, b, c)

  def f(self):
    print('hello')


class C(metaclass=M):
  def g(self):
    print('hello again')

M.x = 8
print(C.x)
C.f()

c = C()
c.g()

