example = 'example' # type 'step'
jump_in = True # now 'display example'

if jump_in:
  example = 'jump'
else:
  example = 'else'

example += ' append' # now try 'rstep'
print(example)
