ppdb - Python debugger with a time machine
==========================================

The goal is to create a clone of `pdb`, but with couple of new commands. For now only `rstep` is implemented, which, as a reversed `step` command, takes you one step back.

To get things working type `python ./run_pdb.py demo.py`. There are no external dependencies and the code was tested all the way down to Python 3.2.

Project status: barely alpha

What doesn't work?
==================

- `import os`
- And by extension, Django, Flask and basically anything of practical value
