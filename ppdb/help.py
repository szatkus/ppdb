import sys
from pdb import Pdb


if sys.version_info.minor == 2 or sys.version_info.minor == 3:
  help_message = """
Documented commands (type help <topic>):
========================================
EOF    cl         disable  interact  next     return  u          where
a      clear      display  j         p        retval  unalias  
alias  commands   down     jump      pp       run     undisplay
args   condition  enable   l         print    rv      unt      
b      cont       exit     list      q        s       until    
break  continue   h        ll        quit     source  up       
bt     d          help     longlist  r        step    w        
c      debug      ignore   n         restart  tbreak  whatis   

Miscellaneous help topics:
==========================
pdb  exec
"""

if sys.version_info.minor >= 4:
  help_message = """
Documented commands (type help <topic>):
========================================
EOF    c          d        h         list      q        rv       undisplay
a      cl         debug    help      ll        quit     s        unt      
alias  clear      disable  ignore    longlist  r        source   until    
args   commands   display  interact  n         restart  step     up       
b      condition  down     j         next      return   tbreak   w        
break  cont       enable   jump      p         retval   u        whatis   
bt     continue   exit     l         pp        run      unalias  where    

Miscellaneous help topics:
==========================
pdb  exec
"""

def get_main_help():
  return help_message

def get_help_for(topic):
  return getattr(Pdb, 'do_' + topic).__doc__
