import sys
import os
import logging
from types import CodeType
from ppdb.debugger import Debugger
from ppdb.help import get_main_help, get_help_for

logging.basicConfig(level=logging.INFO)

if len(sys.argv) == 1:
  print("""usage: ppdb.py [-c command] ... [-m module | pyfile] [arg] ...       

Debug the Python program given by pyfile. Alternatively,
an executable module or package to debug can be specified using     
the -m switch.

Initial commands are read from .pdbrc files in your home directory  
and in the current directory, if they exist.  Commands supplied with
-c are executed after commands from .pdbrc files.

To let the script run until an exception occurs, use "-c continue". 
To let the script run up to a given line X in the debugged file, use
"-c 'until X'".""")
  exit(0)

entry_point = os.path.abspath(sys.argv[1]).lower()
file = open(entry_point, encoding='utf8')
source_code = file.read()
sys.path[0] = dirname = os.path.dirname(entry_point)
file.close()
codeobj = compile(source_code, entry_point, 'exec')
code_lines = source_code.split('\n')
command = ''
debugger = Debugger(entry_point, codeobj)

sources = {
  entry_point: code_lines
}

class Display:
  def __init__(self, expression, old_value):
    self.expression = expression
    self.old_value = old_value

displays = []
watched_frame = None

def show_location(filename, lineno, name):
  print('> %s(%d)%s()' % (filename, lineno, name))
  if filename not in sources:
    file = open(filename, encoding='utf8')
    source_code = file.read()
    code_lines = source_code.split('\n')
    sources[filename] = code_lines
    file.close()
  print('->', sources[filename][lineno - 1].strip())
  
def after_break():
  if debugger.is_finished():
    print('The program finished and will be restarted')
    debugger.reset()
  filename, lineno, name = debugger.get_state()
  show_location(filename, lineno, name)
  if watched_frame != debugger.active_frame:
      del displays[:]
  for display in displays:
    result = debugger.eval(display.expression)
    print('display %s: %s  [old: %s]' % (display.expression, result, display.old_value))
    display.old_value = result

filename, lineno, name = debugger.get_state()
show_location(filename, lineno, name)
while command != 'exit':
  print('(Pdb) ', end='')
  command = input().strip()
  found = False

  if command == 'continue' or command == 'cont' or command == 'c':
    debugger.cont()
    after_break()
    found = True

  if command == 'step' or command == 's':
    result = debugger.step()
    if result == 'call':
      print('--Call--')
    after_break()
    found = True

  if command == 'rstep' or command == 'rs':
    debugger.rstep()
    after_break()
    found = True

  if command.find('b ') == 0 or command.find('break ') == 0:
    filename, lineno = command.split()[1].split(':')
    index, path, lineno = debugger.register_breakpoint(dirname + '/' + filename, lineno)
    print('Breakpoint %d at %s:%d' % (index, path, lineno))
    found = True

  if command.find('display ') == 0:
    expression = command[8:].strip()
    result = debugger.eval(expression)
    display = Display(expression, result)
    if watched_frame != debugger.active_frame:
      del displays[:]
    watched_frame = debugger.active_frame
    displays.append(display)
    print('display %s: %s' % (expression, result))
    found = True

  if command == 'exit':
    found = True

  if command == 'help':
    print(get_main_help())
    found = True

  if command.find('help ') == 0:
    _, topic = command.split()
    print(get_help_for(topic).strip())
    found = True

  if not found:
    print("*** NameError: name '%s' is not defined" % command)

  
