import logging

class AddEvent:

  def __init__(self, obj, attr):
    self.obj = obj
    self.attr = attr

  def __repr__(self):
    return 'AddEvent %s' % (self.attr)

class ChangeEvent:

  def __init__(self, obj, attr, value):
    self.obj = obj
    self.attr = attr
    self.value = value

  def __repr__(self):
    return 'ChangeEvent %s = %s' % (self.attr, self.value)

class DeleteEvent:

  def __init__(self, obj, attr, value):
    self.obj = obj
    self.attr = attr
    self.value = value

  def __repr__(self):
    return 'DeleteEvent %s = %s' % (self.attr, self.value)

class AppendEvent:

  def __init__(self, obj):
    self.obj = obj

  def __repr__(self):
    return 'AppendEvent'

class PopEvent:

  def __init__(self, obj, value):
    self.obj = obj
    self.value = value

  def __repr__(self):
    return 'PopEvent %s' % self.value

class TimeMachine:
  def __init__(self):
    self.events = []

  def register_append(self, obj):
    self.events.append(AppendEvent(obj))

  def register_pop(self, obj, value):
    self.events.append(PopEvent(obj, value))

  def register_add(self, obj, attr):
    self.events.append(AddEvent(obj, attr))

  def register_change(self, obj, attr, value):
    self.events.append(ChangeEvent(obj, attr, value))

  def register_delete(self, obj, attr, value):
    self.events.append(DeleteEvent(obj, attr, value))

  def reverse_change(self):
    event = self.events.pop()
    logging.debug(event)
    if isinstance(event, (ChangeEvent, DeleteEvent)):
      if isinstance(event.obj, dict):
        event.obj[event.attr] = event.value
      else:
        setattr(event.obj, event.attr, event.value)
    if isinstance(event, AddEvent):
      if isinstance(event.obj, dict):
        del event.obj[event.attr]
      else:
        delattr(event.obj, event.attr)
    if isinstance(event, AppendEvent):
      event.obj.pop()
    if isinstance(event, PopEvent):
      event.obj.append(event.value)
    