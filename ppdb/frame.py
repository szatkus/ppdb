import logging
import inspect
from types import CodeType

from ppdb.bytecode import to_bytecode
from ppdb.time_machine import TimeMachine
from ppdb.interpreter import Interpreter, Try, Except

class Frame:

  def __init__(self, filename: str, codeobj: CodeType, interpreter: Interpreter, callback=lambda x: x, globals={}, locals={}, name='', is_module=False, clazz=None, time_machine=TimeMachine()):
    self.filename = filename
    self.varnames = codeobj.co_varnames
    self.args_count = codeobj.co_argcount
    self.cellvars = codeobj.co_cellvars
    self.pc = 0
    self.lineno = codeobj.co_firstlineno
    self.codeobj = codeobj
    self.flags = codeobj.co_flags
    self.bytecode = to_bytecode(codeobj)
    self.globals = globals
    self.locals = locals.copy()
    self.name = name
    self.ext = 0
    self.finished = False
    self.exception = None
    self.interpreter = interpreter
    self.stack = []
    self.blocks = []
    self.callback = callback
    self.time_machine = time_machine
    self.clazz = clazz
    if is_module:
      self.name = '<module>'
      self.locals = self.globals
    self.locals['__name__'] = name
    self.is_module = is_module
    self.labels = {}
    for index, instruction in enumerate(self.bytecode):
      self.labels[instruction.offset] = index
    
  def execute(self):
    while not self.is_finished():
      self.execute_line()

  def execute_line(self):
    while not self.is_finished():
      instruction = self.bytecode[self.pc]
      if instruction.starts_line is not None and instruction.starts_line != self.lineno:
        self.lineno = instruction.starts_line
        logging.debug('%s:%s' % (self.filename, self.lineno))
        return 'newline'
      self.time_machine.register_change(self, 'pc', self.pc)
      self.pc += 1
      
      logging.debug('Stack: %s', self.stack)
      logging.debug('Locals: %s', self.locals)
      logging.debug('Op: %s (%s)', instruction.opname, instruction.argval)
      try:
        if self.ext == 0:
          result = self.interpreter.execute(self, instruction.opname, instruction.argval)
        else:
          ext = self.ext
          self.time_machine.register_change(self, 'ext', self.ext)
          self.ext = 0
          result = self.interpreter.execute(self, instruction.opname, ext * 256 + instruction.argval)
        if result == 'call':
          return 'call'
        if result == 'import':
          return 'import'
      except Exception as e:
        self.raise_exception(e)
        return 'error'

  def is_generator(self):
    return self.flags & inspect.CO_GENERATOR

  def is_coroutine(self):
    return self.flags & 128 # this needs to be hardcoded in case of an older version of Python

  def get_class(self):
    return self.clazz

  def get_self(self):
    return self.locals[self.varnames[0]]

  def raise_exception(self, exception):
    while len(self.blocks) > 0:
      block = self.blocks.pop()
      self.time_machine.register_pop(self.blocks, block)
      if isinstance(block, Try):
        self.time_machine.register_append(self.stack)
        self.stack.append(None) # apparently there must be something else
        self.time_machine.register_append(self.stack)
        self.stack.append(None) # apparently there must be something else
        self.time_machine.register_append(self.stack)
        self.stack.append(None) # apparently there must be something else
        self.time_machine.register_append(self.stack)
        self.stack.append(None) # apparently there must be something else
        self.time_machine.register_append(self.stack)
        self.stack.append(exception)
        self.time_machine.register_append(self.stack)
        if hasattr(exception, 'clazz'):
          self.stack.append(exception.clazz)
        else:
          self.stack.append(exception.__class__)
        self.time_machine.register_change(self, 'pc', self.pc)
        self.pc = self.labels[block.pos]
        self.time_machine.register_append(self.blocks)
        self.blocks.append(Except(-1))
        return
    self.time_machine.register_change(self, 'exception', self.exception)
    self.exception = exception
    self.time_machine.register_change(self, 'finished', self.finished)
    self.finished = True

  def revert_line(self):
    while self.pc > 0:
      self.time_machine.reverse_change()
      instruction = self.bytecode[self.pc]
      if instruction.starts_line is not None and instruction.starts_line != self.lineno:
        self.lineno = instruction.starts_line
        return self.get_state()

  def finish(self):
    self.callback(self)

  def get_state(self):
    return self.filename, self.lineno, self.name

  def is_finished(self):
    return self.finished or self.pc >= len(self.bytecode)

  def is_exploited(self):
    return self.pc >= len(self.bytecode)

  def pass_args(self, args, defaults, kwargs):
    i = 0
    for i in range(min(self.args_count, len(args))):
      self.locals[self.varnames[i]] = args[i]
    for i in range(len(args), self.args_count):
      self.locals[self.varnames[i]] = defaults[-(self.args_count - i)]
    i = self.args_count
    if self.flags & inspect.CO_VARARGS:
      self.locals[self.varnames[i]] = args[self.args_count:]
      i += 1
    if self.flags & inspect.CO_VARKEYWORDS:
      self.locals[self.varnames[i]] = kwargs
      i += 1

  def reset(self):
    self.pc = 0
    self.lineno = self.codeobj.co_firstlineno
    self.globals = {}
    self.locals = {}
