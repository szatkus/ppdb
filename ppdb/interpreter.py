import dis
import imp
import builtins
import sys
import os
import logging

from types import CodeType, FunctionType

from ppdb.time_machine import TimeMachine

class Function:
  def __init__(self, filename: str, code: CodeType, bind=None, clazz=None, defaults=[]):
    self.filename = filename
    self.code = code
    self.bind = bind
    self.clazz = clazz
    self.defaults = defaults

class Try:
  def __init__(self, pos):
    self.pos = pos

class Finally:
  pass

class Except:
  def __init__(self, pos):
    self.pos = pos

class Loop:
  def __init__(self, pos):
    self.pos = pos

class Module:
  def __init__(self):
    self.__all__ = []

class EmptyFrame:
  def __init__(self, result, exception):
    self.result = result
    self.exception = exception

  def is_generator(self):
    return False

  def is_exploited(self):
    return True

def exception_match(a, b):
  if a == b:
    return True
  if isinstance(b, type) and isinstance(a, b):
    return True
  return False

def bind_function(func, instance):
  return Function(func.filename, func.code, bind=instance, clazz=func.clazz, defaults=func.defaults)

def unbind_function(func):
  return Function(func.filename, func.code, bind=None, clazz=func.clazz, defaults=func.defaults)

def get_super(interpreter):
  clazz = interpreter.env.get_class()
  self = interpreter.env.get_self()
  return lambda: super(clazz, self)
  

adapters = {
  'locals': lambda x: lambda: x.env.locals,
  'globals': lambda x: lambda: x.env.globals,
  'super': lambda x: get_super(x)
}

class Interpreter:

  def __init__(self, state, time_machine: TimeMachine):
    self.state = state
    self.time_machine = time_machine

  def set_env(self, env):
    self.env = env

  def execute(self, env, code, arg):
    self.set_env(env)
    method = getattr(self, code)
    return method(arg)

  def add_to_stack(self, value):
    self.time_machine.register_append(self.env.stack)
    self.env.stack.append(value)
    return value

  def pop_stack(self):
    value = self.env.stack.pop()
    self.time_machine.register_pop(self.env.stack, value)
    return value

  def set_local(self, name, value):
    self.time_machine.register_change(self.env.locals, name, self.env.locals.get(name))
    self.env.locals[name] = value

  def get_local(self, name):
    return self.env.locals[name]

  def is_in_locals(self, name):
    return name in self.env.locals

  def delete_locals(self, name):
    self.time_machine.register_delete(self.env.locals, name, self.env.locals[name])
    del self.env.locals[name]

  def get_global(self, name):
    return self.env.globals[name]

  def set_global(self, name, value):
    self.env.globals[name] = value

  def is_in_globals(self, name):
    return name in self.env.globals

  def jump(self, offset):
    self.env.pc = self.env.labels[offset]

  def get_current_path(self):
    return os.path.abspath(self.env.filename)

  def add_block(self, block):
    self.time_machine.register_append(self.env.blocks)
    self.env.blocks.append(block)

  def pop_block(self):
    block = self.env.blocks.pop()
    self.time_machine.register_pop(self.env.blocks, block)
    return block

  def handle_result(self, env):
    if env.exception:
      raise env.exception
    self.add_to_stack(env.result)

  def LOAD_FAST(self, name):
    return self.LOAD_NAME(name)

  def LOAD_NAME(self, name):
    if self.is_in_locals(name):
      self.add_to_stack(self.get_local(name))
    else:
      self.LOAD_GLOBAL(name)

  def LOAD_GLOBAL(self, name):
    if self.is_in_globals(name):
      self.add_to_stack(self.get_global(name))
    elif name in adapters:
      self.add_to_stack(adapters[name](self))
    elif hasattr(builtins, name):
      obj = getattr(builtins, name)
      self.add_to_stack(obj)
    else:
      raise NameError()

  def STORE_LOCALS(self, name):
    locals = self.pop_stack()
    self.time_machine.register_change(self.env, 'locals', self.env.locals)
    self.env.locals = locals

  def LOAD_CONST(self, expr):
    self.add_to_stack(expr)

  def STORE_NAME(self, name):
    value = self.pop_stack()
    self.set_local(name, value)

  def STORE_FAST(self, name):
    value = self.pop_stack()
    self.set_local(name, value)

  def STORE_GLOBAL(self, name):
    value = self.pop_stack()
    self.set_global(name, value)

  def MAKE_FUNCTION(self, flags_or_argc):
    func_name = self.pop_stack()
    if isinstance(func_name, CodeType):
      code = func_name
    else:
      code = self.pop_stack()
    defaults = []
    if sys.version_info.minor < 6:
      for _ in range(flags_or_argc):
        defaults.append(self.pop_stack())
      defaults.reverse()
    else:
      if flags_or_argc & 6:
        raise Exception('TODO')
      if flags_or_argc & 1:
        defaults = self.pop_stack()
      if flags_or_argc & 8:
        cells = self.pop_stack()
    func = Function(self.env.filename, code, defaults=defaults)
    self.add_to_stack(func)

  def CALL_FUNCTION(self, args_count):
    kwargs = {}
    if sys.version_info.minor < 6:
      kwargs_count = args_count // 256
      args_count = args_count % 256
      names = []
      values = []
      for i in range(kwargs_count):
        value = self.pop_stack()
        values.append(value)
        key = self.pop_stack()
        names.append(key)
      names.reverse()
      values.reverse()
      kwargs = { k: v for k, v in zip(names, values) }
    args = []
    for i in range(args_count):
      value = self.pop_stack()
      args.append(value)
    args.reverse()
    
    func = self.pop_stack()

    self.state.call_function(func, self.handle_result, args=args, kwargs=kwargs)
    return 'call'

  def CALL_FUNCTION_EX(self, flags):
    assert flags == 1
    kwargs = self.pop_stack()
    args = self.pop_stack()
    func = self.pop_stack()

    self.state.call_function(func, self.handle_result, args=args, kwargs=kwargs)
    return 'call'

  def CALL_FUNCTION_KW(self, args_count):
    names = self.pop_stack()
    values = []
    for i in range(len(names)):
      value = self.pop_stack()
      values.append(value)
    kwargs = { k: v for k, v in zip(names, reversed(values)) }
    args = []
    for i in range(args_count - len(names)):
      value = self.pop_stack()
      args.append(value)
    args.reverse()
    
    func = self.pop_stack()

    self.state.call_function(func, self.handle_result, args=args, kwargs=kwargs)
    return 'call'

  def POP_TOP(self, _):
    value = self.pop_stack()

  def DUP_TOP(self, _):
    tos = self.pop_stack()
    self.add_to_stack(tos)
    self.add_to_stack(tos)

  def NOP(self, _):
    pass

  def ROT_TWO(self, _):
    first = self.pop_stack()
    second = self.pop_stack()
    self.add_to_stack(first)
    self.add_to_stack(second)

  def ROT_THREE(self, _):
    first = self.pop_stack()
    second = self.pop_stack()
    third = self.pop_stack()
    self.add_to_stack(first)
    self.add_to_stack(third)
    self.add_to_stack(second)

  def ROT_FOUR(self, _):
    first = self.pop_stack()
    second = self.pop_stack()
    third = self.pop_stack()
    fourth = self.pop_stack()
    self.add_to_stack(first)
    self.add_to_stack(fourth)
    self.add_to_stack(third)
    self.add_to_stack(second)

  def POP_JUMP_IF_FALSE(self, offset):
    if not self.pop_stack():
      self.jump(offset)

  def POP_JUMP_IF_TRUE(self, offset):
    if self.pop_stack():
      self.jump(offset)

  def JUMP_FORWARD(self, offset):
    self.jump(offset)

  def JUMP_IF_NOT_EXC_MATCH(self, offset):
    b = self.pop_stack()
    a = self.pop_stack()
    if not exception_match(a, b):
      self.jump(offset)

  def COMPARE_OP(self, opcode):
    b = self.pop_stack()
    a = self.pop_stack()
    op = dis.cmp_op[opcode]
    if op == '<':
      self.add_to_stack(a < b)
    elif op == '<=':
      self.add_to_stack(a <= b)
    elif op == '==':
      self.add_to_stack(a == b)
    elif op == '!=':
      self.add_to_stack(a != b)
    elif op == '>':
      self.add_to_stack(a > b)
    elif op == '>=':
      self.add_to_stack(a >= b)
    elif op == 'in':
      self.add_to_stack(a in b)
    elif op == 'not in':
      self.add_to_stack(a not in b)
    elif op == 'is':
      self.add_to_stack(a is b)
    elif op == 'is not':
      self.add_to_stack(a is not b)
    elif op == 'exception match':
      if exception_match(a, b):
        self.add_to_stack(True)
      else:
        self.add_to_stack(False)
    elif op == 'BAD':
      raise Exception("WHAT?")

  def BINARY_RSHIFT(self, _):
    b = self.pop_stack()
    a = self.pop_stack()
    self.add_to_stack(a >> b)

  def BINARY_LSHIFT(self, _):
    b = self.pop_stack()
    a = self.pop_stack()
    self.add_to_stack(a << b)

  def BINARY_AND(self, _):
    b = self.pop_stack()
    a = self.pop_stack()
    self.add_to_stack(a & b)

  def BINARY_OR(self, _):
    b = self.pop_stack()
    a = self.pop_stack()
    self.add_to_stack(a | b)

  def BINARY_XOR(self, _):
    b = self.pop_stack()
    a = self.pop_stack()
    self.add_to_stack(a ^ b)

  def BINARY_ADD(self, _):
    b = self.pop_stack()
    a = self.pop_stack()
    self.add_to_stack(a + b)

  def BINARY_SUBTRACT(self, _):
    b = self.pop_stack()
    a = self.pop_stack()
    self.add_to_stack(a - b)

  def BINARY_TRUE_DIVIDE(self, _):
    b = self.pop_stack()
    a = self.pop_stack()
    self.add_to_stack(a / b)

  def BINARY_MULTIPLY(self, _):
    b = self.pop_stack()
    a = self.pop_stack()
    self.add_to_stack(a * b)

  def BINARY_MODULO(self, _):
    b = self.pop_stack()
    a = self.pop_stack()
    self.add_to_stack(a % b)

  def BINARY_FLOOR_DIVIDE(self, _):
    b = self.pop_stack()
    a = self.pop_stack()
    self.add_to_stack(a // b)

  def INPLACE_ADD(self, _):
    self.BINARY_ADD(_)

  def RETURN_VALUE(self, _):
    if hasattr(self.env, 'result'):
      self.time_machine.register_change(self.env, 'result', self.env.result)
    else:
      self.time_machine.register_add(self.env, 'result')
    self.env.result = self.pop_stack()
    self.time_machine.register_change(self.env, 'finished', self.env.finished)
    self.env.finished = True
    self.add_to_stack(self.env.result) # needed for generators

  def YIELD_VALUE(self, _):
    self.RETURN_VALUE(_)

  def IMPORT_NAME(self, name):
    names = self.pop_stack()
    level = self.pop_stack()
    dir_path = None
    if level > 0:
      dir_path = os.path.dirname(self.get_current_path())
      for i in range(level - 1):
        dir_path = os.path.normpath(os.path.join(dir_path, '..'))
    try:
      if dir_path:
        reader, path, _ = imp.find_module(name, [dir_path])
      else:
        reader, path, _ = imp.find_module(name)
    except ImportError as e:
      if name in sys.modules:
        self.add_to_stack(sys.modules[name])
        return
      raise e
    def callback(env):
      if env.exception:
        raise env.exception
      module = Module()
      for name, value in env.locals.items():
        try:
          setattr(module, name, value)
          module.__all__.append(name)
        except AttributeError as e:
          logging.warn(e)
      self.add_to_stack(module)
    if reader is None and name in sys.builtin_module_names:
      module = __import__(name)
      self.add_to_stack(module)
    else:
      self.state.do_import(path, callback)
      return 'import'

  def IMPORT_FROM(self, name):
    module = self.pop_stack()
    self.add_to_stack(module)
    try:
      self.add_to_stack(getattr(module, name))
    except AttributeError:
      raise ImportError("cannot import name '%s'" % name)

  def IMPORT_STAR(self, name):
    module = self.pop_stack()
    for attr in dir(module):
      if attr.find('_') != 0:
        self.set_local(attr, getattr(module, attr))

  def CONTAINS_OP(self, invert):
    iterable = self.pop_stack()
    value = self.pop_stack()
    if invert:
      self.add_to_stack(value not in iterable)
    else:
      self.add_to_stack(value in iterable)

  def BUILD_MAP(self, count):
    if count > 0:
      raise Exception('TODO')
    self.add_to_stack(dict())

  def BUILD_TUPLE(self, length):
    result = []
    for _ in range(length):
      result.append(self.pop_stack())
    result.reverse()
    self.add_to_stack(tuple(result))

  def BUILD_SET(self, length):
    result = []
    for _ in range(length):
      result.append(self.pop_stack())
    result.reverse()
    self.add_to_stack(set(result))

  def BUILD_CONST_KEY_MAP(self, argc):
    names = self.pop_stack()
    assert argc == len(names)
    result = {}
    for name in reversed(names):
      result[name] = self.pop_stack()
    self.add_to_stack(result)

  def LIST_APPEND(self, i):
    value = self.pop_stack()
    self.time_machine.register_append(self.env.stack[-i])
    list.append(self.env.stack[-i], value)

  def SET_ADD(self, i):
    value = self.pop_stack()
    self.time_machine.register_append(self.env.stack[-i])
    set.add(self.env.stack[-i], value)

  def BUILD_LIST(self, length):
    result = []
    for _ in range(length):
      result.append(self.pop_stack())
    result.reverse()
    self.add_to_stack(result)

  def LOAD_BUILD_CLASS(self, _):
    
    def build_class(func, name, *superclasses, **kwargs):
      metaclass = kwargs.get('metaclass')
      def callback(env):
        definition = env.locals.copy()

        if metaclass:
          self.state.call_function(metaclass.__new__, finish, args=(metaclass, name, superclasses, definition))
        else:
          if sys.version_info.minor < 4:
            clazz = __build_class__(lambda d: d.update(definition), name, *superclasses, **kwargs)
          else:
            if '__classcell__' in definition:
              del definition['__classcell__']
            globs = {'definition': definition}
            globs.update(globals())
            f = FunctionType(compile('for _ppkey, _ppvalue in definition.items():\n  locals()[_ppkey] = _ppvalue', '', 'exec'), globals=globs)
            clazz = __build_class__(f, name, *superclasses, **kwargs)
          for key in dir(clazz):
            if hasattr(clazz, key):
              value = getattr(clazz, key)
              if isinstance(value, Function):
                value.clazz = clazz
          finish(EmptyFrame(clazz, None))

      def finish(env):
        if env.exception:
          raise env.exception
        clazz = env.result
        
        if metaclass:
          for key in dir(metaclass):
            if hasattr(metaclass, key):
              class_value = getattr(clazz, key)
              value = getattr(metaclass, key)
              if isinstance(value, Function) and class_value == value:
                setattr(clazz, key, bind_function(value, metaclass))

        def init(self, *args, **kwargs):
          for key in dir(self):
            value = object.__getattribute__(self, key)
            if isinstance(value, Function):
              value = Function(value.filename, value.code, bind=self, clazz=value.clazz)
              setattr(self, key, value)

        clazz.__init__ = init

        self.add_to_stack(clazz)

      args = []
      if sys.version_info.minor < 4:
        args = [{'__name__': '__main__'}] # FIXME
      self.state.call_function(func, callback, args=args)
    self.add_to_stack(build_class)

  def LOAD_METHOD(self, name):
    obj = self.pop_stack()
    self.add_to_stack(obj)
    method = getattr(obj, name)
    if isinstance(method, Function):
      method = unbind_function(method)
      if isinstance(obj, super):
        method.bind = obj.__self__
    self.add_to_stack(method)

  def LOAD_ATTR(self, name):
    obj = self.pop_stack()
    attr = getattr(obj, name)
    if isinstance(attr, Function) and isinstance(obj, super):
      attr = bind_function(attr, obj.__self__)
    self.add_to_stack(attr)

  def STORE_ATTR(self, name):
    obj = self.pop_stack()
    value = self.pop_stack()
    setattr(obj, name, value)

  def CALL_METHOD(self, args_count):
    args = []
    for i in range(args_count):
      value = self.pop_stack()
      args.append(value)
    args.reverse()
    
    func = self.pop_stack()

    self_arg = self.pop_stack()

    self.state.call_function(func, self.handle_result, self_arg=self_arg, args=args)
    return 'call'

  def SETUP_EXCEPT(self, pos):
    self.add_block(Try(pos))

  def RAISE_VARARGS(self, args_count):
    assert args_count == 1
    exception = self.pop_stack()
    self.env.raise_exception(exception)

  def FOR_ITER(self, pos):
    iterator = self.pop_stack()
    def callback(env):
      if env.is_generator() and env.is_exploited():
        self.jump(pos)
      if exception_match(env.exception, StopIteration):
        self.jump(pos)
      elif env.exception is None:
        self.add_to_stack(iterator)
        self.add_to_stack(env.result)
      else:
        raise env.exception
    self.state.call_function(iterator.__next__, callback)
    return 'call'

  def JUMP_ABSOLUTE(self, pos):
    self.jump(pos)

  def JUMP_IF_FALSE_OR_POP(self, pos):
    value = self.pop_stack()
    if not value:
      self.jump(pos)
      self.add_to_stack(value)

  def BINARY_SUBSCR(self, _):
    index = self.pop_stack()
    value = self.pop_stack()
    self.add_to_stack(value[index])

  def STORE_SUBSCR(self, _):
    index = self.pop_stack()
    array = self.pop_stack()
    value = self.pop_stack()
    array[index] = value

  def GET_ITER(self, _):
    i = self.pop_stack()
    self.state.call_function(i.__iter__, lambda x: self.add_to_stack(x.result))
    return 'call'

  def BUILD_SLICE(self, argc):
    if argc == 2:
      a = self.pop_stack()
      b = self.pop_stack()
      self.add_to_stack(slice(b, a))
      return
    if argc == 3:
      a = self.pop_stack()
      b = self.pop_stack()
      c = self.pop_stack()
      self.add_to_stack(slice(c, b, a))
      return
    raise Exception('Bad!')

  def SETUP_LOOP(self, pos):
    self.add_block(Loop(pos))

  def SETUP_FINALLY(self, pos):
    self.add_block(Try(pos))

  def BEGIN_FINALLY(self, _):
    self.add_block(Finally())
    self.add_to_stack(None)

  def POP_BLOCK(self, pos):
    self.pop_block()
  
  def POP_EXCEPT(self, pos):
    assert isinstance(self.pop_block(), Except)
    self.pop_stack()
    self.pop_stack()
    self.pop_stack()

  def DELETE_NAME(self, name):
    self.delete_locals(name)

  def END_FINALLY(self, _):
    if sys.version_info.minor > 7:
      self.pop_block()
    tos = self.pop_stack()
    if tos is None:
      return
    elif isinstance(tos, int):
      self.jump(tos)
    elif issubclass(tos, Exception):
      exception = self.pop_stack()
      self.pop_stack()
      self.pop_stack()
      self.pop_stack()
      self.pop_stack()
      raise exception

  def LOAD_CLOSURE(self, i):
    self.add_to_stack(self.env.cellvars[i])

  def MAKE_CLOSURE(self, i):
    if sys.version_info.minor > 2:
      name = self.pop_stack() # FIXME do something with the name
    codeobj = self.pop_stack()
    self.add_to_stack(None) # there should be a tuple
    self.add_to_stack(Function(codeobj.co_filename, codeobj))

  def EXTENDED_ARG(self, i):
    self.time_machine.register_change(self.env, 'ext', self.env.ext)
    self.env.ext = self.env.ext * 256 + i

  def UNPACK_SEQUENCE(self, count):
    pack = self.pop_stack()
    if len(pack) != count:
      self.env.raise_exception(ValueError('cannot unpack'))
      return
    for element in reversed(pack):
      self.add_to_stack(element)
    

