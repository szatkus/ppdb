import os
import logging
from types import CodeType

from ppdb.frame import Frame
from ppdb.time_machine import TimeMachine
from ppdb.interpreter import Interpreter, EmptyFrame

class Generator:

  def __init__(self, frame, handler):
    self.frame = frame
    self.handler = handler

  def __iter__(self):
    return self

  def __next__(self, callback):
    if not self.frame.is_exploited():
      self.handler(self.frame, callback)
    else:
      callback(EmptyFrame(None, StopIteration()))

Generator.__next__.__internal__ = True

class Coroutine:

  def __init__(self, frame, handler):
    self.frame = frame
    self.handler = handler

  def close(self):
    pass

globals_map = {}

class Debugger:

  def __init__(self, filename: str, codeobj: CodeType):
    self.time_machine = TimeMachine()
    normalized_path = self.normalize_path(filename)
    if normalized_path not in globals_map:
        globals_map[normalized_path] = {}
    self.base_frame = Frame(normalized_path, codeobj, Interpreter(self, self.time_machine), globals=globals_map[normalized_path], is_module=True, time_machine=self.time_machine)
    self.active_frame = self.base_frame
    self.frames = [self.active_frame]
    self.breakpoints = set()
    self.started = False
    self.finished = False
    self.modules = {}

  def call_function(self, func, callback, args=[], kwargs={}, self_arg=None):
    args = [a for a in args]
    kwargs = kwargs.copy()
    if hasattr(func, '__internal__'):
      func(*args, **{'callback': callback})
    elif hasattr(func, '__call__'):
      try:
        result = func(*args, **kwargs)
        callback(EmptyFrame(result, None))
      except Exception as e:
        callback(EmptyFrame(None, e))
    else:
      if func.bind:
        args.insert(0, func.bind)
      elif self_arg:
        args.insert(0, self_arg)
      normalized_path = self.normalize_path(func.filename)
      if normalized_path not in globals_map:
        globals_map[normalized_path] = {}
      new_frame = Frame(normalized_path, func.code, Interpreter(self, self.time_machine), globals=globals_map[normalized_path], clazz=func.clazz, callback=callback, name=func.code.co_name, time_machine=self.time_machine)
      new_frame.pass_args(tuple(args), func.defaults, kwargs)
      if new_frame.is_generator():
        generator = Generator(new_frame, self.handle_frame)
        callback(EmptyFrame(generator, None))
      elif new_frame.is_coroutine():
        generator = Coroutine(new_frame, self.handle_frame)
        callback(EmptyFrame(generator, None))
      else:
        self.handle_frame(new_frame, callback)
        

  def cont(self):
    if not self.started:
      self.started = True
      if self.active_frame.get_state()[:2] in self.breakpoints:
        return self.active_frame.get_state()

    while not self.active_frame.is_finished():
      result = self.active_frame.execute_line()

      if not self.active_frame.is_finished() and result == 'newline' and self.active_frame.get_state()[:2] in self.breakpoints:
        return self.active_frame.get_state()

      self.resolve()

    return self.active_frame.get_state()

  def eval(self, expression):
    codeobj = compile(expression, '', 'eval')
    frame = Frame('', codeobj, Interpreter(self, self.time_machine), globals=self.active_frame.globals, locals=self.active_frame.locals)
    frame.execute()
    return frame.result

  def get_state(self):
    return self.active_frame.get_state()

  def handle_frame(self, frame, callback):
    self.time_machine.register_change(frame, 'finished', frame.finished)
    frame.finished = False
    self.time_machine.register_change(frame, 'callback', frame.callback)
    frame.callback = callback
    self.time_machine.register_append(self.frames)
    self.frames.append(frame)
    self.time_machine.register_change(self, 'active_frame', self.active_frame)
    self.active_frame = frame

  def is_finished(self):
    return self.base_frame.is_finished()

  def normalize_path(self, path):
    return os.path.abspath(path).lower()

  def register_breakpoint(self, filename, lineno):
    breakpoint = (self.normalize_path(filename), int(lineno))
    self.breakpoints.add(breakpoint)
    return (len(self.breakpoints),) + breakpoint

  def reset(self):
    self.base_frame.reset()
    self.active_frame = self.base_frame
    self.frames = [self.active_frame]
  
  def do_import(self, path, callback):
      path = self.normalize_path(path)
      if path not in self.modules:
        logging.debug('Import %s' % path)
        if os.path.isdir(path):
          path = os.path.join(path, '__init__.py')
        file = open(path, encoding='utf8')
        source_code = file.read()
        file.close()
        codeobj = compile(source_code, path, 'exec')
        if path not in globals_map:
          globals_map[path] = {}
        new_frame = Frame(path, codeobj, Interpreter(self, self.time_machine), callback=callback, is_module=True, time_machine=self.time_machine, globals=globals_map[path])
        self.time_machine.register_append(self.frames)
        self.frames.append(new_frame)
        self.time_machine.register_change(self, 'active_frame', self.active_frame)
        self.active_frame = new_frame
        self.time_machine.register_change(self.modules, path, self.modules.get(path))
        self.modules[path] = new_frame
      else:
        callback(self.modules[path])

  def resolve(self):
    if self.active_frame.is_finished() and len(self.frames) > 1:
      finished_frame = self.frames.pop()
      self.time_machine.register_pop(self.frames, finished_frame)
      assert finished_frame == self.active_frame
      self.time_machine.register_change(self, 'active_frame', self.active_frame)
      self.active_frame = self.frames[-1]
      finished_frame.finish()

    if self.base_frame.exception:
      raise self.base_frame.exception

    return False

  def rstep(self):
    if self.active_frame.pc != 0:
      self.active_frame.revert_line()
    else:
      active_frame = self.active_frame
      while active_frame == self.active_frame:
        self.time_machine.reverse_change()
    

  def step(self):

    return self.active_frame.execute_line()

