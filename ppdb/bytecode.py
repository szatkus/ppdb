import dis
import sys
import logging
from types import CodeType

class Instruction:
  def __init__(self, opname, argval, starts_line, offset):
    self.opname = opname
    self.argval = argval
    self.starts_line = starts_line
    self.offset = offset

  def __repr__(self):
    return 'Op %s(%s) at %s' % (self.opname, self.argval, self.starts_line)

def to_bytecode(codeobj: CodeType):
  HAVE_ARGUMENT = 90 # this seems to be the same since Python 3.2

  i = 0
  code = codeobj.co_code
  result = []
  linestarts = { offset: line for offset, line in dis.findlinestarts(codeobj) }
  while i < len(code):
    opcode = int(code[i])
    if opcode >= HAVE_ARGUMENT:
      arg = int(code[i + 1])
      if sys.version_info.minor < 6 and int(code[i + 2]) != 0:
        arg += int(code[i + 2]) * 256
      
      if opcode in dis.hasconst:
        argval = codeobj.co_consts[arg]
      elif opcode in dis.hasname:
        argval = codeobj.co_names[arg]
      elif opcode in dis.haslocal:
        argval = codeobj.co_varnames[arg]
      elif opcode in dis.hasjrel:
        if sys.version_info.minor < 6:
          argval = arg + i + 3
        else:
          argval = arg + i + 2
      else:
        argval = arg
      instruction = Instruction(dis.opname[opcode], argval, linestarts.get(i), i)
      result.append(instruction)
      logging.debug('%d: %s', i, instruction)
      if sys.version_info.minor < 6:
        i += 3
      else:
        i += 2
    else:
      instruction = Instruction(dis.opname[opcode], None, linestarts.get(i), i)
      result.append(instruction)
      logging.debug('%d: %s', i, instruction)
      if sys.version_info.minor < 6:
        i += 1
      else:
        i += 2
  return result
