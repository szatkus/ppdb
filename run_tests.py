import os
from subprocess import Popen, PIPE
import sys

def execute_test(filename):
  print(filename)
  if filename[-4].isdigit() and sys.version_info.minor < int(filename[-4]):
    return
  source_file = filename.replace('.in', '.py')
  commands = list(open(filename))
  ppdb_process = Popen([sys.executable, 'run_pdb.py', source_file], stdout=PIPE, stderr=PIPE, stdin=open(filename))
  ppdb_process.wait()
  if ppdb_process.returncode != 0:
    print(ppdb_process.stdout.read().decode('utf-8'))
    print(ppdb_process.stderr.read().decode('utf-8'))
    raise Exception('error')
  output_file = filename.replace('.in', '.out')
  if os.path.exists(output_file):
    file = open(output_file)
    expectation = file.read().replace('$PATH/', os.path.abspath(os.path.dirname(filename)).lower() + os.path.sep)
    file.close()
  else:
    pdb_process = Popen([sys.executable,  '-m', 'pdb', source_file], stdout=PIPE, stderr=PIPE, stdin=open(filename))
    pdb_process.wait()
    
    expectation = pdb_process.stdout.read().decode('utf-8').strip().replace('\r', '')
  output = ppdb_process.stdout.read().decode('utf-8').strip().replace('\r', '')
  try:
    assert output == expectation
  except AssertionError as e:
    print('###Expected:')
    print(expectation)
    print('###Output:')
    print(output)
    print('###')
    output_lines = output.split('\n')
    expectation_lines = expectation.split('\n')
    for i in range(max(len(output_lines), len(expectation_lines))):
      if output_lines[i] != expectation_lines[i]:
        print('Difference in line %d' % (i + 1))
        print('>>>', expectation_lines[i])
        print(output_lines[i])

    raise e

if len(sys.argv) > 1:
  execute_test(sys.argv[1])
else:
  for root, _, files in os.walk('tests'):
    for filename in files:
      if filename.find('.in') != -1:
        execute_test(os.path.join(root, filename))
