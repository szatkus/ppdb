import sys
import os
import logging

from ppdb.debugger import Debugger

logging.basicConfig(level=logging.INFO)

entry_point = os.path.abspath(sys.argv[1]).lower()
file = open(entry_point, encoding='utf8')
source_code = file.read()
sys.path[0] = dirname = os.path.dirname(entry_point)
file.close()
codeobj = compile(source_code, entry_point, 'exec')
code_lines = source_code.split('\n')
command = ''
debugger = Debugger(entry_point, codeobj)
debugger.cont()
